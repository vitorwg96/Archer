using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterAnimation : MonoBehaviour
{
    private Animator _animator;
    private CharacterCombat _combat;
    

    protected virtual void Start()
    {
        _combat = GetComponent<CharacterCombat>();
        
    }

    protected virtual void Update()
    {
       // _animator.SetFloat("Speed Percent", _navmeshAgent.velocity.magnitude / _navmeshAgent.speed, .1f, Time.deltaTime);
    }

    protected virtual void OnAttack()
    {
        _animator.SetTrigger("Attack");
    }

}
