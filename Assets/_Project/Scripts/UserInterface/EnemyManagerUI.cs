using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyManagerUI : MonoBehaviour
{
    [SerializeField] private Text _enemyCount;
    [SerializeField] private Text _waveCount;

    private void Start()
    {

    }

    private void Update()
    {
        _enemyCount.text = "Remaing Enemy: " + ScoreManager.instance._enemyRemain;
        _waveCount.text = "Wave: " + ScoreManager.instance.GetCountWave();
    }

}
