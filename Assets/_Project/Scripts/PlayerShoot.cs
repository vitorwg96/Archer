using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    [SerializeField]
    private PoolerObject _arrowPooler;
    [SerializeField]
    private Transform _shootPoint;
    [SerializeField]
    private GameObject _followPlayer;

    PlayerController _shootEvent;

    private void Awake()
    {
        _shootEvent = GetComponent<PlayerController>();
        _shootEvent.OnShoot += ShootEvent_OnShoot;
    }

    private void OnDisable()
    {
        _shootEvent.OnShoot -= ShootEvent_OnShoot;
    }


    private void ShootEvent_OnShoot()
    {
        // Shoot

        GameObject newArrow = _arrowPooler.GetObject();
        newArrow.transform.forward = _followPlayer.transform.forward;
        newArrow.transform.position = _shootPoint.position;

        // Add StartCoroutine to give time to object to spawn and tranform
        StartCoroutine(CountToShoot());

        IEnumerator CountToShoot()
        {
            yield return new WaitForSeconds(0.2f);
            newArrow.GetComponent<ArrowProjectile>().Fire();
        }
    }
}
