using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{


    public List<ArrowItem> _arrows = new List<ArrowItem>();

    public void Add (ArrowItem arrow)
    {
        _arrows.Add(arrow);
    }

    public void Remove(ArrowItem arrow)
    {
        _arrows.Remove(arrow);
    }



}
