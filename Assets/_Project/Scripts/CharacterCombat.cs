using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterStats))]
public class CharacterCombat : MonoBehaviour
{
    private float _attackCountdown = 0f;

    private CharacterStats _myStats;
    private CharacterStats _enemyStats;


    private void Start()
    {
        _myStats = GetComponent<CharacterStats>();
    }
    private void Update()
    {
        _attackCountdown -= Time.deltaTime;
    }

    public void Attack(CharacterStats enemyStats)
    {
        if (_attackCountdown <= 0)
        {
            this._enemyStats = enemyStats;

            _attackCountdown = _myStats.GetAttackRateValue();
            // _attackCountdown = 1f / _myStats.GetAttackRateValue(); ;

            StartCoroutine(DoDamage(enemyStats, .6f));
            
            // event OnAttack
        } 
    }

    IEnumerator DoDamage(CharacterStats stats, float delay)
    {
        yield return new WaitForSeconds(delay);

        Debug.Log(transform.name + " swings for " + _myStats.GetDamageValue() + " damage");
        stats.TakeDamage(_myStats.GetDamageValue());
    }


}
