using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Arrows", menuName = "Arrow Item")]
public class ArrowItem : ScriptableObject
{
    new public string name = "New Item";

    public Sprite icon = null;
        
    public bool isDefaultItem = false;
    
}