using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    [SerializeField]
    private float _timeToSpawn = 5f;
    [SerializeField]
    private PoolerObject _objPooler;
    private float _timeSinceSpawn;

    private void Update()
    {
        _timeSinceSpawn += Time.deltaTime;
        if(_timeSinceSpawn >= _timeToSpawn)
        {
            GameObject newObj = _objPooler.GetObject();
            newObj.transform.position = this.transform.position;
            _timeSinceSpawn = 0f;
        }
    }

}
