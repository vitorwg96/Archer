using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class ArrowProjectile : MonoBehaviour
{
    [SerializeField] private CharacterCombat _playerCombat;
    private Rigidbody _rb;
    private PoolerObject _arrowPoller;

    private float _forcePower = 25f;

    public void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        _rb.centerOfMass = transform.position;
    }
    private void Start()
    {
        _arrowPoller = GetComponentInParent<PoolerObject>();
    }

    private void OnEnable()
    {
        _rb.velocity = Vector3.forward;
    }

    public void Fire()
    {
        _rb.AddForce(transform.forward * _forcePower, ForceMode.Impulse);
    }

    // What is Obsolete?
    [System.Obsolete]
    public void OnTriggerEnter(Collider other)
    {
        if (!other.GetComponent<PlayerController>() && other.gameObject.isStatic)
        {
            _rb.isKinematic = true;
            StartCoroutine(Countdown());
        }

        if (other.GetComponent<EnemyController>())
        {
            // The value of the dmg need be a variable, corresponding as the arrow type
            other.GetComponent<CharacterStats>().TakeDamage(1);
            gameObject.GetComponent<CharacterStats>().TakeDamage(1);
            // Need stop object because is continue add a force.

            //other.GetComponentInParent<PoolerObject>().ReturnObject(other.gameObject);
            //other.GetComponent<EnemyController>().Die();
            //gameObject.GetComponent<CharacterCombat>().Attack(other.GetComponent<CharacterStats>());
            //_arrowPoller.ReturnObject(gameObject);
        }


        IEnumerator Countdown()
        {
            yield return new WaitForSeconds(6);
            _arrowPoller.ReturnObject(gameObject);
        }
    }
}
