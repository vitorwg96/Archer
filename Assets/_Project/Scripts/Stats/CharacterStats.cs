using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{

    // Any other class will be able to "get" this value, but we can only "set" this value from within this class
    private int _currentHealth;
    [SerializeField]
    private Stat _damage;
    [SerializeField]
    private Stat _speed;
    [SerializeField]
    private Stat _attackRate;
    [SerializeField]
    private Stat _maxHealth;
    private PoolerObject _pooler;

    public event Action OnDead;
    public event Action OnDamage;

    private void Awake()
    {
        _currentHealth = _maxHealth.GetValue();
    }

    private void Start()
    {
        _pooler = GetComponentInParent<PoolerObject>();
    }

    private void OnEnable()
    {
        _currentHealth = _maxHealth.GetValue();
        Debug.Log("On Enable " + _currentHealth);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            TakeDamage(1);
        }
    }

    public void TakeDamage(int damage)
    {
        _currentHealth -= damage;
        Debug.Log(transform.name + " takes " + damage + " damage.");
        OnDamage?.Invoke();
        Debug.Log("TakeDamage" + _currentHealth);
        if (_currentHealth <= 0)
        {
            Die();
            _currentHealth = _maxHealth.GetValue();
        }
    }

    public virtual void Die()
    {
        if (gameObject.TryGetComponent(out Rigidbody rb))
        {
            rb.velocity = Vector3.zero;
        }
        Debug.Log(transform.name + " died.");
        OnDead?.Invoke();
        _pooler.ReturnObject(gameObject);
    }

    public int GetDamageValue()
    {
        return _damage.GetValue();
    }

    public int GetSpeedValue()
    {
        return _speed.GetValue();
    }

    public int GetAttackRateValue()
    {
        return _attackRate.GetValue();
    }

    public int GetCurrentHealth()
    {
        return _currentHealth;
    }
}
