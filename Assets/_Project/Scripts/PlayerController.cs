using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    private Rigidbody playerRb;
    private PlayerInput playerInput;

    public event Action OnShoot;


    [SerializeField]
    private GameObject followPlayer;
    [SerializeField]
    private GameObject _enemy;

    private Vector2 _direction;
    private Vector2 _look;
    private Quaternion nextRotation;

    // need change this variavel to the same of the arrow equiped
    private float _cooldownToFire = 2f;
    private float _timeSieceFire;

    private float moveSpeed = 5f;
    private float rotateSpeed = -1.5f;

    private void Start()
    {
        // _actionOnTimer.SetTimer(2f, () => { Debug.Log("Fire!"); });
        Cursor.visible = false;
    }

    private void Update()
    {
        _timeSieceFire += Time.deltaTime;

        _direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        _look = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        #region Camera and Movement Controls

        transform.position += transform.forward * _direction.y * Time.deltaTime * moveSpeed;
        transform.position += transform.right * _direction.x * Time.deltaTime * moveSpeed;

        followPlayer.transform.rotation *= Quaternion.AngleAxis(_look.x * rotateSpeed, Vector3.down);
        followPlayer.transform.rotation *= Quaternion.AngleAxis(_look.y * rotateSpeed, Vector3.right);

        var angles = followPlayer.transform.localEulerAngles;
        angles.z = 0;

        var angle = followPlayer.transform.localEulerAngles.x;

        if (angle > 180 && angle < 340)
        {
            angles.x = 340;
        }
        else if (angle < 180 && angle > 40)
        {
            angles.x = 40;
        }

        followPlayer.transform.localEulerAngles = angles;

        //Set the player rotation based on the look transform
        transform.rotation = Quaternion.Euler(0, followPlayer.transform.rotation.eulerAngles.y, 0);
        //reset the y rotation of the look transform
        followPlayer.transform.localEulerAngles = new Vector3(angles.x, 0, 0);
        #endregion

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if(_timeSieceFire >= _cooldownToFire)
            {
                OnShoot?.Invoke();
                _timeSieceFire = 0f;
            }
        }
    }
}