using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//[RequireComponent(typeof(Rigidbody))]
public class EnemyController : MonoBehaviour
{
    [SerializeField]
    private Transform _playerPos;
    [SerializeField] private GameObject _player;
    private NavMeshAgent _agent;
    private CharacterCombat _combat;
    private Animator _animator;
    [SerializeField]
    private PoolerObject _pooler;
    
    private float _lerpRotation = 5f;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        _playerPos = FindObjectOfType<PlayerController>().GetComponent<Transform>();
        _combat = GetComponent<CharacterCombat>();
        _animator = GetComponent<Animator>();
    }

    private void Start()
    {
        _pooler = GetComponentInParent<PoolerObject>();
    }


    private void Update()
    {
        float distance = Vector3.Distance(_playerPos.transform.position, transform.position);
        
        if (distance <= _agent.stoppingDistance)
        {
            _combat.Attack(_player.GetComponent<PlayerStats>());
            FaceTarget();
            _animator.SetBool("isAttack", true);
        }
        //_animator.SetBool("isAttack", false);

        _agent.SetDestination(_playerPos.transform.localPosition);       
    }

    //[System.Obsolete]
    //public void Die()
    //{
    //    _animator.SetTrigger("Die");
    //    _agent.Stop();
    //    StartCoroutine(OnDie());
    //}

    //IEnumerator OnDie()
    //{
    //    yield return new WaitForSeconds(3);

    //    Destroy(gameObject);
    //   // _pooler.ReturnObject(gameObject);
    //}

    private void FaceTarget()
    {
        Vector3 direction = (_playerPos.transform.localPosition - transform.localPosition).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0.0f, direction.z));
        transform.rotation = Quaternion.Slerp(lookRotation, transform.rotation, Time.deltaTime * _lerpRotation);
    }
}
 