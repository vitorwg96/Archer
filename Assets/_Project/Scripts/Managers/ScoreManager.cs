using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance { get; private set; }

    // Fazer depois com enemy Count
    public int _enemyRemain;
    private int _countWave;


    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of ScoreManager found!", gameObject);
            return;
        }
        
        instance = this;
    }

    public void AddCountWave()
    {
        _countWave++;
    }

    public int GetCountWave()
    {
        return _countWave;
    }

}
