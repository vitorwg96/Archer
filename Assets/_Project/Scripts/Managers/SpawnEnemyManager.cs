using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemyManager : MonoBehaviour
{
    [SerializeField]
    private float _spawnRange = 12.0f;
    [SerializeField]
    private Transform[] _spawnPoint;

    private PoolerObject _objPooler;
    private int _enemyCount;
    private int _waveNumber = 1;

    private void Awake()
    {
        _objPooler = GetComponent<PoolerObject>();
    }

    private void Start()
    {
       SpawnEnemyWave(_waveNumber);
    }

    private void Update()
    {
        _enemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;

        // Can i do this singleton to get information? 
        ScoreManager.instance._enemyRemain = _enemyCount;

        if (_enemyCount == 0)
        {
            SpawnEnemyWave(_waveNumber);
        }
    }

    private void SpawnEnemyWave(int enemiesToSpawn)
    {
        for (int i = 0; i < enemiesToSpawn; i++)
        {
            GameObject newObj = _objPooler.GetObject();
            newObj.transform.position = GenerateRadomTransfom();
        }
        // conferir depois
        //ScoreManager.instance._countWave = _waveNumber;
        ScoreManager.instance.AddCountWave();
        _waveNumber++;
    }

    private Vector3 GenerateSpawnPosition()
    {
        float spawnPosX = Random.Range(-_spawnRange, _spawnRange);
        float spawnPosZ = Random.Range(-_spawnRange, _spawnRange);

        Vector3 randomPos = new Vector3(spawnPosX, 0, spawnPosZ);

        return randomPos;
    }

    private Vector3 GenerateRadomTransfom()
    {
        int i = Random.Range(0, _spawnPoint.Length);
        return _spawnPoint[i].position;

    }
}
