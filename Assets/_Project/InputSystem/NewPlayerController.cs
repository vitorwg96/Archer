using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class NewPlayerController : MonoBehaviour
{
    // Get refference of PlayerStats
    [SerializeField] private float _playerSpeed = 2f;

    private InputManager _inputManager;
    private CharacterController _controller;
    private Transform _cameraTransform;


    private void Start()
    {
        _inputManager = InputManager.Instance;
        _controller = GetComponent<CharacterController>();
        _cameraTransform = Camera.main.transform;
    }

    private void Update()
    {
        Vector2 movement = _inputManager.GetPlayerMovement();
        Vector3 move = new Vector3(movement.x, 0f, movement.y);
        move = _cameraTransform.forward * move.z + _cameraTransform.right * move.x;
        move.y = 0;

        _controller.Move(move * Time.deltaTime * _playerSpeed);
    }
}
