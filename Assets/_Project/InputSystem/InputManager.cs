using UnityEngine;

public class InputManager : MonoBehaviour
{
    private static InputManager _instance;

    public static InputManager Instance
    {
        get
        {
            return _instance;
        }
    }
    
    
    private PlayerInputController _inputActions;
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Debug.LogWarning("More than one instance of InputManager found!", gameObject);
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        Cursor.visible = false;

        _inputActions = new PlayerInputController();    
    }
    private void OnEnable()
    {
        _inputActions.Enable();    
    }
    private void OnDisable()
    {
        _inputActions.Disable();    
    }

    public Vector2 GetPlayerMovement()
    {
        return _inputActions.Player.Move.ReadValue<Vector2>();
    }
    public Vector2 GetMouseDelta()
    {
        return _inputActions.Player.Look.ReadValue<Vector2>();
    }

    public bool GetFireAction()
    {
        return _inputActions.Player.Fire.triggered;
    }
}
